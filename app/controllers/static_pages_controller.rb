class StaticPagesController < ApplicationController
    before_action :redireciona, only: [:home]

    def home
    end

    private

    def redireciona
        if logged_in?
            redirect_to current_user
        end
    end
end
