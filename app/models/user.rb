class User < ActiveRecord::Base
    has_many :posts
    has_secure_password
    validates :password, presence: true, length: {minimum: 6}
    has_and_belongs_to_many :friendships,
        class_name: "User", 
        join_table:  :friendships, 
        foreign_key: :user_id, 
        association_foreign_key: :friend_user_id
end
