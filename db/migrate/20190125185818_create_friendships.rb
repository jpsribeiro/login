class CreateFriendships < ActiveRecord::Migration[5.2]
  def change
    create_table :friendships, id: false do |t|
      t.references :user
      t.references :friend

      t.timestamps
    end
    add_index(:friendships, [:user_id, :friend_user_id], :unique => true)
    add_index(:friendships, [:friend_user_id, :user_id], :unique => true)
  end
end
